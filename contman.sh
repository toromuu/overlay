#!/usr/bin/env bash

help() {
    echo "Usage: " >&2

    echo "[-d | --deploy ] [ipsubred1] [ipsubred2] [Ncontainers]            deploy all the scenario, 2 nodes with subred, and containers"
    echo "[-dn | --deployNode ] [ipsubred1] [Ncontainers]                   deploy the network only in the node selected "
    echo "[-add | --addContainer ] [N1|N2] [ipcontainer]                    add one container in the node selected with a ip-adress  "
    echo "[-rmc | --removeContainer ] [N1|N2] [NameSpaceofContainer]        remove a container selected by Namespace in one Node "
    echo "[--rn | --removeNetwork ] [N1|N2]                                 remove the network of selected Node "
    echo "[--rvm | --removeVMs ]                                            remtove all the VM "
    echo "[--int | --installVagrant ]                                       install all components for VM virtualization"

    exit 1
}


getSubnet () {

  if [[ $argument =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\/[0-9]+$ ]]; then

  IFS=/ read IP PREFIX <<< $argument
  IFS=. read -r i1 i2 i3 i4 <<< $IP
  IFS=. read -r xx m1 m2 m3 m4 <<< $(for a in $(seq 1 32); do if [ $(((a - 1) % 8)) -eq 0 ]; then echo -n .; fi; if [ $a -le $PREFIX ]; then echo -n 1; else echo -n 0; fi; done)

  result=$(echo "$((i1 & (2#$m1))).$((i2 & (2#$m2))).$((i3 & (2#$m3))).$((i4 & (2#$m4)))")
  resultprefix=$PREFIX

  else
    echo "$argument Wrong ip-adress format"
    help
  fi

}


setupVM () {

  cd ./ESCENARIO_4

  sudo rm -rf ./Vagrantfile &>/dev/null

  #Sensible to spaces and /n /t

echo "#-*- mode: ruby -*-
# vi: set ft=ruby :

"'$script = <<SCRIPT'"
echo ""cd /vagrant"" >> /home/vagrant/.bashrc
echo ""ln -sf /vagrant/.vim /home/vagrant/.vim"" >> /home/vagrant/.bashrc
echo ""ln -sf /vagrant/.vimrc /home/vagrant/.vimrc"" >> /home/vagrant/.bashrc
export DEBIAN_FRONTEND=noninteractive
SCRIPT

Vagrant.configure(2) do |config|
  config.vm.define "'"'$nodo1'"'"  do |n1|
      n1.vm.box = "'"'$distribucion'"'"
      n1.vm.hostname = "'"'$nodo1'"'"
      n1.vm.network :private_network, ip:" '"'$rednodo1'"'", virtualbox__intnet: "'"'$networkname'"'"
      n1.vm.provision " '"'"shell"'"'", inline: "'$script'"
  end
  config.vm.define "'"'$nodo2'"'"  do |n2|
      n2.vm.box = "'"'$distribucion'"'"
      n2.vm.hostname = "'"'$nodo2'"'"
      n2.vm.network :private_network, ip:" '"'$rednodo2'"'", virtualbox__intnet: "'"'$networkname'"'"
      n2.vm.provision " '"'"shell"'"'", inline: "'$script'"
  end
  config.vm.provision :docker
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
end
" >> ./Vagrantfile

  vagrant up
  vagrant ssh $nodo1 -c 'cd /vagrant ; sudo bash E4.sh --deploy '$IpRedNodo1' '$prefixRedNodo1' '$ncontainerspernode' '$rednodo2' '$IpRedNodo2' '$prefixRedNodo2' ;'
  vagrant ssh $nodo2 -c 'cd /vagrant ; sudo bash E4.sh --deploy '$IpRedNodo2' '$prefixRedNodo2' '$ncontainerspernode' '$rednodo1' '$IpRedNodo1' '$prefixRedNodo1' ;'


}




#-- GLOBAL VARIABLES

#-- VM settings
DefaultIpSubRedNodo1="192.168.111.0/24"
DefaultIpSubRedNodo2="192.168.112.0/24"
nodo1="Escenario4Nodo1"
nodo2="Escenario4Nodo2"
rednodo1="10.0.0.10"
rednodo2="10.0.0.20"
distribucion="ubuntu/xenial64"
networkname="Escenario4Network"
ncontainerspernode=2


if [[ $EUID -ne 0 ]]; then
    echo "El script se debe ejecutar con permisos de root"
    exit 1
fi


# --MAIN CONTROLLER

while :
do
    case "$1" in

      -h | --help)
          help
          exit 0
          ;;

      -d | --deploy)
            if [[ $# -eq 4 ]] ; then
              argument=$2
              getSubnet
              IpRedNodo1=$result
              prefixRedNodo1=$resultprefix
              argument=$3
              getSubnet
              IpRedNodo2=$result
              prefixRedNodo2=$resultprefix
              ncontainerspernode=$4

              echo "Deploying default scenario with N1-$IpRedNodo1 N2-$IpRedNodo2 $4 containers per node  "
              setupVM
              exit 0

            else
              IpRedNodo1=$DefaultIpSubRedNodo1
              IpRedNodo2=$DefaultIpSubRedNodo2

              echo "Not enough argument"
              echo "Deploying default scenario with N1-$IpRedNodo1 N2-$IpRedNodo2 $4 containers per node "
              setupVM
              exit 0

            fi
          ;;

       -dn | --deployNode)
             if [[ $2 == "N1" ]]; then

               nodoSelected=$nodo1

             elif [[ $2 == "N2" ]]; then

               nodoSelected=$nodo2

             else
               echo "Wrong argument $2"
               help
             fi

             ncontainerspernode=$4
             argument=$3
             getSubnet
             echo "Deploying network in $2"
             cd ./ESCENARIO_4
             vagrant ssh $nodoSelected -c 'cd /vagrant ; sudo bash E4.sh --deploy '$result' '$resultprefix' '$ncontainerspernode' ;'
             exit 0
              ;;

      -add | --addContainer)
            if [[ $2 == "N1" ]]; then

              nodoSelected=$nodo1

            elif [[ $2 == "N2" ]]; then

              nodoSelected=$nodo2

            else
              echo "Wrong argument $2"
              help
            fi

            argument=$3
            getSubnet
            echo "Deploying one container more in $2"
            cd ./ESCENARIO_4
            vagrant ssh $nodoSelected -c 'cd /vagrant ; sudo bash E4.sh --addContainer '$result' '$resultprefix';'
            exit 0
          ;;

      -rmc | --removeContainer)

            if [[ $2 == "N1" ]]; then

              nodoSelected=$nodo1

            elif [[ $2 == "N2" ]]; then

              nodoSelected=$nodo2

            else
              echo "Wrong argument $2"
              help
            fi

            echo "Removing container $3 in $2"
            cd ./ESCENARIO_4
            vagrant ssh $nodo1 -c 'cd /vagrant ; sudo bash E4.sh --removeContainer '$3';'
            exit 0


          ;;


        -rn | --removeNetwork )
            if [[ $2 == "N1" ]]; then

              nodoSelected=$nodo1

            elif [[ $2 == "N2" ]]; then

              nodoSelected=$nodo2

            else
              echo "Wrong argument $2"
              help
            fi

            echo "Removing Network in $2"
            cd ./ESCENARIO_4
            vagrant ssh $nodo1 -c 'cd /vagrant ; sudo bash E4.sh --removeNetwork;'
            exit 0
          ;;

      -rvm | --removeVMs )
                echo "removing VMs"
                  cd ./ESCENARIO_4/
                  sudo vagrant destroy --force
                  sudo rm -rf .vagrant/
                  sudo rm -rf ubuntu-xenial-16.04-cloudimg-console.log
                  sudo rm -rf ./Vagrantfile
                  exit 0
          ;;

      -int | --installVagrant )

              curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
              sudo apt-get install virtualbox
              sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
              sudo apt-get update && sudo apt-get install vagrant
              sudo vagrant plugin install vagrant-vbguest
              exit 0
              ;;

      -*)
          echo "Error: Unknown option: $1" >&2
          display_help
          exit 1
          ;;
    esac
done
