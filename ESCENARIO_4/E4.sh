#!/usr/bin/env bash


# AUXILIAR FUNCTIONS



# --------------------------------------------------------
# REMOVE
# --------------------------------------------------------
# Stop and remove Container set in variable NS_CONTAINER
removeContainer(){

  sudo aa-remove-unknown
  sudo docker rm -f ${NS_CONTAINER}
  # Remove connections and namespace associated
  sudo unlink /var/run/netns/${NS_CONTAINER}
  sudo ip netns del ${NS_CONTAINER}

}


# Remove Namespace Brige set in variable BR_DEV
removeBridge(){

  sudo ip netns del ${BR_DEV}

}

# Controller for remove all container + connection
cleanVM() {

  ip netns list | while read -r namespace ; do

    NS_CONTAINER=$namespace
    removeContainer

  done
  removeBridge

}



# --------------------------------------------------------
# CREATE
# --------------------------------------------------------

# Build container with docker
createContainer() {

  # build docker image from dockerfile
  sudo docker build -t e1 .

  # raises 2 containers from the image created before, without VETH
  # Note the --network none option
  sudo docker run -dit --rm --name ${NS_CONTAINER} --network none --cap-add=NET_ADMIN --cap-add=NET_RAW e1 /bin/sh

}

# Create a Bridge interface
createBridge (){

  # setup bridge
  sudo ip link add ${BR_DEV} type bridge

  # setup bridge ip
  IPBR=$(echo "${BR_ADDR}/${PREFIX}")
  sudo ip addr add ${IPBR} dev ${BR_DEV}

  # echo "DEBUGING BRIDGE"
  # echo "$IPBR"
  sudo ip link set dev ${BR_DEV} up
  # "Setting the route on the node to reach the network namespaces on the other node"



}


#Create Veth -- Vpeer connection
# .----------.vpeer       .--------.
# | container|------------| bridge |
# '----------'       veth' --------'

createPairConnection() {

  # docker does not add container network namespaces to the linux runtime data (/var/run mounted as a tmpfs from /run)
  # we have to do it manually
  # create a special space where add containers namespaces
  mkdir -p /var/run/netns &>/dev/null

  # Gets the PID of each container
  c1_pid="$(docker inspect --format '{{.State.Pid}}' ${NS_CONTAINER})"

  # Mount the namespaces of each container
  sudo ln -s /proc/$c1_pid/ns/net /var/run/netns/${NS_CONTAINER}

  # create veth <--> vepeer link
  sudo ip link add ${VETH} type veth peer name ${VPEER}

  # setup veth link
  sudo ip link set ${VETH} up

  # add veth--vpeer pair to namespace
  sudo ip link set ${VPEER} netns ${NS_CONTAINER}

  # setup loopback interface
  sudo ip netns exec ${NS_CONTAINER} ip link set lo up

  # setup peer ns interface
  sudo ip netns exec ${NS_CONTAINER} ip link set ${VPEER} up

  # assign ip address to ns interfaces
  IPVPER=$(echo "${VPEER_ADDR}/${PREFIX}")
  # echo "DEBUGIN createPairConnection"
  # echo "$IPVPER"

  sudo ip netns exec ${NS_CONTAINER} ip addr add ${IPVPER} dev ${VPEER}

  # assign veth pairs to bridge
  sudo ip link set ${VETH} master ${BR_DEV}


  # add default routes for namespace
  sudo ip netns exec ${NS_CONTAINER} ip route add default via ${BR_ADDR}


}


# --------------------------------------------------------
# CONTROLLERS
# --------------------------------------------------------



deployScenario(){

  createBridge

  for i in $( seq 1 $N_CONTAINERS )
  do

    NS_CONTAINER="$NS_CONTAINERa$i"
    VETH="${VETHa}$i"
    VPEER="${VPEERa}$i"
    let n=$i+$cuart
    VPEER_ADDR=${ADDRa::-1}$n

    createContainer
    createPairConnection

  done

  N2BRIDGE=$(echo "${N2BRIDGESUBNET}/${N2BRIDGEDPREFIX}")
  # echo "DEBUGING"
  # echo "$N2SUBRED"
  # echo "${N2BRIDGE}"

  sudo ip route add ${N2BRIDGE} via ${N2SUBRED} dev enp0s8

  # # enable ip forwarding
  # bash -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'
  sudo bash -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'
  sudo bash -c 'echo 1 > /proc/sys/net/ipv4/conf/all/forwarding'

  # # Flush nat rules.
   # sudo iptables -t nat -F
  # echo "DEBUGING Flush nat rules."
   sudo iptables -t nat -A POSTROUTING -s ${IPBR} ! -o ${BR_DEV} -j MASQUERADE

   # sudo iptables -t nat -A PREROUTING ! -s 10.0.0.0/24 -p tcp -m tcp --dport 80 -j DNAT --to-destination 10.0.0.2

   # sudo iptables -t nat -A OUTPUT -d 192.168.111.0 -p tcp -m tcp --dport 80 -j DNAT --to-destination 10.0.0.2


}



deployContainer(){

  # Determinate N actual containers using namespaces
  let Nactual=$(ip netns list | wc -l)+1

  # Auto-defaultNames
  NS_CONTAINER="$NS_CONTAINERa$Nactual"
  VETH="${VETHa}$Nactual"
  VPEER="${VPEERa}$Nactual"

  # Asign a IP available
  let n=$Nactual+$cuart
  VPEER_ADDR=${ADDRa::-1}$n

  createContainer
  createPairConnection

}


# --------------------------------------------------------
# MAIN PROGRAM
# --------------------------------------------------------


# GLOBAL VARIABLES

# ---DEFAULT NAMES

# Container Name
NS_CONTAINERa="NS_CONTAINER"
# Pair VETH--VPEER
VETHa="VETH"
VPEERa="VPEER"



# ---IP AVAILABLE

# Split SUBRED/PREFIX
PREFIX=$3
#IFS=/ read IP PREFIX <<< $2
ADDRa=$2
# Get the last prefix xxx.xxx.xxx.xxx
arrIN=(${2//./ })
let cuart=${arrIN[3]}+1

# Bridge IP --> first ip available
BR_ADDR=${ADDRa::-1}$cuart
BR_DEV="BR_0"

# Ncontenedores
N_CONTAINERS=$4

# Other Node SUBRED
N2SUBRED=$5

# Other Bridge Subnet
N2BRIDGESUBNET=$6
N2BRIDGEDPREFIX=$7



# echo "DEBUGING"
#
# echo "$1 $2 $3 $4 $5 $6 $7"
#
# echo "$BR_ADDR"



while :
do
    case "$1" in

      -h | --help)
          display_help
          exit 0
          ;;

      -d | --deploy)

            deployScenario

            exit 0
          ;;


      -add | --addContainer)

            deployContainer
            exit 0

          ;;

      -rm | --removeContainer)

            NS_CONTAINER=$2
            removeContainer
            exit 0

          ;;


        -rc | --removeNetwork )
            cleanVM
            exit 0
          ;;

      -*)
          echo "Error: Unknown option: $1" >&2
          display_help
          exit 1
          ;;
    esac
done
