# OVERLAY

> Implentation aproach by D.Toro for CC_asignature task

## Introduction

One of the mechanisms used by container systems such as Docker is the separation of namespaces, so that each container receives its own namespace for each of the named resources it has access to in the OS. Of special relevance is the network namespace, as it is not possible to properly isolate a container if it is allowed to communicate arbitrarily with any other agent in a distributed system. Containers in Linux use network namespaces to establish proper network isolation.

## Description

The purpose of the following repository is to emulate a network scenario with the following characteristics, contained in 4 scenarios. Each scenario is incremental covering the features of the previous scenarios.


> SCENARIO 1

- R1. On each node containers will be launched from Docker images, which, initially, will not be networked
  (they do not get a Docker VETH).
- R2. The containers deployed on each node can communicate with each other.
- R3. Communication exists between Host <---> Containers

> SCENARIO 2

- R4. Containers can communicate with any node reachable from the host on your local area network.
- R5. From nodes that can reach the host, communication can be established with a container.

> SCENARIO 3

- R6. Two nodes on the same local area network, containers are launched on both nodes.
- R7. Containers are seen on the same layer 2 network (same Ethernet LAN).

> SCENARIO 4

- R8. Two nodes on the same LAN, containers are launched on both nodes, communication
from containers to the internet.

We can see graphically the scenario displayed in the following diagram.

<div align="center">
	<img width="821" src="readme/Scenario.PNG">
</div>

## Stack

1. Ubuntu 18 (host OS)
2. Bash
3. Vagrant
4. Virtual Box
5. Docker (inside VM deployed )
6. Alpine (as SO container )
6. Iptables (inside container deployed )


## Requirements

  SOFTWARE:
  + Vagrant
  + Virtual Box
  + Vagrant-vbguest

  You can install these tools by running the script contman.sh with the following option:
  ```
  sudo ./contman.sh --installVagrant  
  ```

## Setting up the project

Open the terminal and get the source code.

    git clone https://gitlab.com/toromuu/overlay.git

Change the permissions of contman.sh

    sudo chmod 777 contman.sh

Change the permissions of E4.sh

    cd ./ESCENARIO_4
    sudo chmod 777 E4.sh


## How to use it

1. If you want to deploy the default scenario, detailed above, i.e. 2 nodes with 2 containers on each node, under ip address 192.168.111.0/24. Alternatively you can specify the subnet for each node and the number of initial containers for each node.

        sudo ./contman.sh --deploy [ipsubred1] [ipsubred2] [Ncontainers]

    where
    + ipsubred1 : subnet for node 1, e.g. 192.168.112.0/24
    + ipsubred2 : subnet for node 2, e.g. 192.168.111.0/24
    + Ncontainers : number of containers initially deployed on each node, e.g, 3

    example:

        sudo ./contman.sh --deploy 192.168.130.0/24 192.168.140.0/24 3

    If you dont use it any argument, by default ipsubred1=192.168.111.0/24 ipsubred2=192.168.112.0/24 Ncontainers=2

    You can check the status of each VM :

        cd ./ESCENARIO_4
        sudo vagrant status

    You can connect to each node via:

        sudo vagrant ssh Escenario4Nodo1 # To connect NODE1
        sudo vagrant ssh Escenario4Nodo2 # To connect NODE2



2. This option is used to display the network on the selected node only. It is useful when clearing the network configuration of a node, and instead of turning the virtual machine off and on again, you just reconfigure the machine.

        sudo ./contman.sh --deployNode [N1|N2]] [ipsubred1] [Ncontainers]

    example:

        sudo ./contman.sh --deployNode N1 192.168.111.0/24 4

3. Once the nodes are deployed, you can deploy a container on the node of your choice indicating its ip address.
Note that it is possible that the address you indicate is already occupied by other containers or by the bridge, in which case the next available one of the indicated subnet will be assigned, for example, indicating 192.168.111.3/24 with an initial configuration of 2 containers, the 192.168.111.4 will be assigned to the third container indicated.

        sudo ./contman.sh -add | --addContainer ] [N1|N2] [ipcontainer]  

    where
    +  N1 or N2 : is one of the two available nodes
    +  ipcontainer : the ip address you want to give to the new container

     example:

        sudo ./contman.sh  --addContainer N2 192.168.111.4/24

4. If you want to remove a container selected by Namespace in one Node

        sudo ./contman.sh --removeContainer [N1|N2] [NameSpaceofContainer]  

    where
    +   N1 or N2 : is one of the two available nodes
    +   NameSpaceofContainer: the identifier of the container to be deleted

    example:

        sudo ./contman.sh --removeContainer N1 NS_CONTAINER2


5. If you want to remove the network of selected Node.Remove containers, pair VETH--VPEER connections, namespaces, routing table.

       sudo ./contman.sh --removeNetwork [N1|N2]                                

6. If you want to remove the virtual machines, shut down the virtual machines and remove all their generated data and files.

        sudo ./contman.sh --removeVMs


7. If you want to install all components for VM virtualization.

        sudo ./contman.sh --installVagrant

## Testing

> ESCENARIO 1

- R1-The following statement ensures that each container is lifted without a VETH connector.

        docker run -dit --rm --name  --network none --cap-add=NET_ADMIN --cap-add=NET_RAW e1 /bin/sh

     Pay attention to the options --network none  and --cap-add=NET_RAW

- R2-You can access each container by means of, as appropriate:

        docker exec -it [NS_CONTAINER1|NS_CONTAINER2] /bin/sh

    You can check the communication with another container by ping
    For example, for container1 with ip-adress 192.168.111.2:

        docker exec -it NS_CONTAINER1 /bin/sh
        ping 192.168.111.3 #to check communication with container 2

- R3-To check communication container1 --> host

        ping 10.0.0.10

- To check communication host --> container1
    from Escenario4Nodo1 VM

        ping 192.168.111.2


> ESCENARIO 2

- R4-R5-R6 To check communication of the containers in the other node
     from Escenario4Nodo2 VM

        ping 192.168.112.2

> ESCENARIO 3

-   R7 It is guaranteed by the use of brige


> ESCENARIO 4

- R8 To check communication to internet

        ping 8.8.8.8






## RESOURCES

http://blog.arunsriraman.com/2017/03/container-namespaces-how-to-add.html

http://blog.arunsriraman.com/2017/03/container-namespaces-how-to-add.html

https://developers.redhat.com/blog/2018/10/22/introduction-to-linux-interfaces-for-virtual-networking#vlan

https://medium.com/techlog/diving-into-linux-networking-and-docker-bridge-veth-and-iptables-a05eb27b1e72

https://blog.quarkslab.com/digging-into-linux-namespaces-part-1.html

https://platform9.com/blog/container-namespaces-deep-dive-container-networking/

https://iximiuz.com/en/posts/networking-lab-simple-vlan/

https://iximiuz.com/en/posts/container-networking-is-simple/

https://iximiuz.com/en/posts/networking-lab-ethernet-broadcast-domains/

https://stackoverflow.com/questions/60536501/bash-script-to-dynamically-create-virtual-network-interfaces

https://itnext.io/create-your-own-network-namespace-90aaebc745d

https://www.teldat.com/blog/es/nftables-and-netfilter-hooks-via-linux-kernel/

https://leftasexercise.com/2020/01/17/virtual-networking-labs-building-a-virtual-firewall-and-router-with-linux-namespaces/

https://ops.tips/blog/using-network-namespaces-and-bridge-to-isolate-servers/

https://leftasexercise.com/2020/01/17/virtual-networking-labs-building-a-virtual-firewall-and-router-with-linux-namespaces/

https://dev.to/faayam/understanding-container-networking-using-linux-network-namespaces-and-a-virtual-switch-to-isolate-servers-2k0c
